﻿using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public new Rigidbody2D rigidbody;
    public float speed = 12f;

    private Vector2 input;
    //private Vector2 lastFacing;

    public bool IsFacingRight => Quaternion.Dot(transform.rotation, Quaternion.identity) > 0.99F;
    public bool IsFacingLeft => Quaternion.Dot(transform.rotation, Quaternion.identity) < 0.01F;

    private void FixedUpdate()
    {
        var hasInput = Mathf.Abs(input.sqrMagnitude) > 0.01F;
        if (hasInput)
        {
            var velocity = input * speed * Time.deltaTime;
            //rigidbody.position += velocity;
            rigidbody.MovePosition(rigidbody.position + velocity);
        }
    }

    public void Move(Vector2 input)
    {
        //lastFacing = this.input.normalized;
        this.input = input;

        var isInputingRight = this.input.x > 0F;
        var isInputingLeft = this.input.x < 0F;
        var shouldTurnHorizontally =
            isInputingRight && IsFacingLeft ||
            isInputingLeft && IsFacingRight;

        if (shouldTurnHorizontally) TurnHorizontally();
    }

    public void TurnHorizontally()
    {
        transform.Rotate(Vector3.up, 180F);
    }
}
