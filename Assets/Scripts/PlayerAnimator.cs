﻿using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    public Animator animator;

    private Vector2 lastDirection = Vector2.down;

    public void Move(Vector2 input, bool hasHorizontalInput, bool hasVerticalInput)
    {
        var hasInput = Mathf.Abs(input.sqrMagnitude) > 0F;

        if (hasHorizontalInput) lastDirection = new Vector2(input.x, 0F);
        if (hasVerticalInput) lastDirection = new Vector2(0F, input.y);

        animator.SetBool("hasInput", hasInput);
        animator.SetFloat("horizontalInput", input.x);
        animator.SetFloat("horizontalDirection", lastDirection.x);
        animator.SetFloat("verticalDirection", lastDirection.y);
        animator.SetFloat("verticalInput", input.y);
    }
}
