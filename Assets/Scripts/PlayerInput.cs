﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public PlayerMotor motor;
    public PlayerAnimator animator;

    // Update is called once per frame
    void Update()
    {
        var input = new Vector2(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical")
            );

        var hasHorizontalInput = Mathf.Abs(input.x) > 0F;
        var hasVerticalInput = Mathf.Abs(input.y) > 0F;
        var hasBothInput = hasHorizontalInput && hasVerticalInput;

        if (hasBothInput) input.x = 0F;

        motor.Move(input);
        animator.Move(input, hasHorizontalInput, hasVerticalInput);
    }
}
